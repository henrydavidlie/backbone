//
//  BackboneApp.swift
//  Backbone
//
//  Created by Henry David Lie on 13/07/21.
//

import SwiftUI

@main
struct BackboneApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
